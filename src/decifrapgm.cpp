#include "decifrapgm.hpp"
#include <string>

using namespace std;

DecifraPGM::DecifraPGM()
{
  setNumeroMagico("P5");
  setAltura("512");
  setLargura("512");
  setValorMaximoDaCor("255");
}

void DecifraPGM::setValorInicial (int valorInicial)
{
  this-> valorInicial = valorInicial;
}

int DecifraPGM::getValorInicial()
{
  return valorInicial;
}

void DecifraPGM::setCaracterExtraido (char caracter_extraido)
{
  this-> caracter_extraido = caracter_extraido;
}

char DecifraPGM::getCaracterExtraido()
{
  return caracter_extraido;
}

void DecifraPGM::setFinalArquivo (string finalArquivo)
{
  this-> finalArquivo = finalArquivo;
}

string DecifraPGM::getFinalArquivo()
{
  return finalArquivo;
}

void DecifraPGM::setBitExtraido (char bit_extraido)
{
  this-> bit_extraido = bit_extraido;
}

char DecifraPGM::getBitExtraido()
{
  return bit_extraido;
}

void DecifraPGM::setByteArquivo (char byte_arquivo)
{
  this-> byte_arquivo = byte_arquivo;
}

char DecifraPGM::getByteArquivo()
{
  return byte_arquivo;
}

char DecifraPGM::decifrar(char byte_arquivo)
{
  while (8)
  {

     bit_extraido = byte_arquivo & 0x01;
     caracter_extraido = caracter_extraido | (bit_extraido << 1);

  }
  return this->caracter_extraido;
}

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <sstream>

#include "imagem.cpp"
#include "decifrapgm.cpp"
//#include "decifrappm.cpp"

using namespace std;

int main () {

    ifstream arquivoImagem;

    string numero_magico;
    string altura;
    string largura;
    string valor_maximo_da_cor;
    string nome_da_imagem;


    Imagem * imagem;
    DecifraPGM * imagemPGM;
    //DecifraPPM * imagemPPM ;

    int escolha_menu_principal;
    //int escolha_cor;
    int alturaEmInteiro;
    int larguraEmInteiro;

    std::stringstream converteAlturaEmInteiro (altura);
    std::stringstream converteLarguraEmInteiro (largura);


    std::cout << "Digite o nome da imagem" << std::endl;
    std::cin >> nome_da_imagem;

do{

    std::cout << "Escolha uma das opções abaixo:" << std::endl;
    std::cout << "1 : Abrir imagem" << std::endl;
    std::cout << "2 : Decifrar imagem" << std::endl;
    std::cout << "3 : Encerrar" << std::endl;

    cin >> escolha_menu_principal;


  switch (escolha_menu_principal)
   {

    case 1: arquivoImagem.open(nome_da_imagem.c_str());

      if(!arquivoImagem.is_open( ))
        {
          cout<<"Não foi possível abrir a imagem! Finalizando programa...\n";
          exit(1);
          arquivoImagem.clear( );
        }
        else {
          std::cout << "Arquivo aberto com sucesso!" << std::endl << std::endl;
          arquivoImagem.clear( );
        }
    break;

    case 2:

          arquivoImagem >> numero_magico;
          arquivoImagem.seekg(42,arquivoImagem.beg);
          arquivoImagem >> largura;
          arquivoImagem.seekg(46,arquivoImagem.beg);
          arquivoImagem >> altura;
          arquivoImagem.seekg(50,arquivoImagem.beg);
          arquivoImagem >> valor_maximo_da_cor;


  imagemPGM = new DecifraPGM ();
  //imagemPPM = new DecifraPPM ();
  imagem = new Imagem(numero_magico, altura, largura, valor_maximo_da_cor);

     if ( !(converteAlturaEmInteiro >> alturaEmInteiro))
        alturaEmInteiro = 0;
     if ( !(converteLarguraEmInteiro >> larguraEmInteiro))
        larguraEmInteiro = 0;

     if (numero_magico == "P5")
        {
          arquivoImagem.seekg(50000,arquivoImagem.cur);
      while(!arquivoImagem.eof())
         {
           imagemPGM->decifrar(arquivoImagem.get());
         }
        }

      else
        { /*

        std::cout << "Escolha uma das opções abaixo:" << std::endl;
        std::cout << "1 : Vermelho" << std::endl;
        std::cout << "2 : Verde" << std::endl;
        std::cout << "3 : Azul" << std::endl;

        cin >> escolha_cor;

        switch (escolha_cor)
     {
      case 1: imagemPPM->decifrar(1);
      case 2: imagemPPM->decifrar(2);
      case 3: imagemPPM->decifrar(3);
    }

      */}

  arquivoImagem.close();

  break;

    case 3: std::cout << "Encerrando ..." << std::endl; exit(1);

    }
} while (escolha_menu_principal);

  delete (imagem);
  delete (imagemPGM);
  //delete (imagemPPM);

  return 0;

}

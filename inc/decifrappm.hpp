#ifndef DECIFRAPPM_HPP
#define DECIFRAPPM_HPP

#include "decifra.hpp"
#include <string>


class DecifraPPM : public Decifra {

private:

  int escolha_da_cor;

public:

    DecifraPPM();

    void decifrar( int escolha_da_cor);
};
#endif

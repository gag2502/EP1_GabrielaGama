#ifndef DECIFRAPGM_HPP
#define DECIFRAPGM_HPP

#include "decifra.hpp"
#include <string>

class DecifraPGM : public  Decifra {

private:

   int valorInicial;

   char caracter_extraido ;
   char bit_extraido;
   char byte_arquivo;

   string finalArquivo;

public:
  
     DecifraPGM();

     void setValorInicial (int valorInicial);
     int getValorInicial();
     void setCaracterExtraido (char caracter_extraido);
     char getCaracterExtraido();
     void setBitExtraido (char bit_extraido);
     char getBitExtraido();
     void setByteArquivo (char byte_arquivo);
     char getByteArquivo();
     void setFinalArquivo (string finalArquivo);

     string getFinalArquivo();
     char decifrar(char byte_arquivo);


};
#endif

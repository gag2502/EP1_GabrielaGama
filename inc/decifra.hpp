#ifndef DECIFRA_HPP
#define DECIFRA_HPP

#include "imagem.hpp"
#include <string>

class Decifra : public Imagem {

    public:

        Decifra ();

    protected:

        string decifrar(string numero_magico);

};
#endif

Nome: Gabriela Alves da Gama
Matrícula : 150127006

Para executar o programa deve-se abrir o executável main na pasta /bin. A pasta fica no diretório principal do projeto.
A compilação deve ser feita pelo Makefile, utilizando o comando make no terminal do Linux dentro do diretório principal do projeto.
Ao abrir a imagem deve-se digitar o nome e a extensão do arquivo. Exemplo:

-> lena.pgm

Observações: Ao compilar algumas vezes o projeto, observei que entre uma compilação e outra o compilador acusava o erro de "multipla definição" ou "referência indefinida". Consegui vencer esses erros por comentar as classes com múltipla definição, logo após recompilar, daí o erro apresentado foi o de referência indefinida. Em seguida, apaguei o comentário das classes e recompilei. Dessa forma tudo voltou ao normal. Não consigo explicar o porquê desses erros. 
